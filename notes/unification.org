* Unification Problem.

- The first order unification problem

You have two functional symbols f and g.
f is of arity 2 and g is of arity 3.


f(x, g(u,f(y,x),w) ) ≡ f(g(a,b,c), z)

(1) x = g(a,b,c)
(2) z = g(u,f(y,x)


Type system:

constants, type variables and ->

Constants as function symbols of arity 0
-> as the function symbol of arity 2.


τ₁ ≡ τ₂


Γ ⊢ e₁ : τ₁ -> τ₂
Γ ⊢ e₂ : τ₁
==================
Γ ⊢ e₁ e₂ : τ₂


In the inference algorithm when you encounter the term e₁ e₂

Recursively infer the type of e₁ to get say t₁  e₂ to get t₂

We will have to solve the following equations

t₁ ≡ α -> β
t₂ ≡ α


1. The notion of a signature.

   A set Σ of functional symbols together with their arity.

2. Terms(Σ)

   - A variable is a term
   - if t₁,....,tₙ are terms and f is a functional symbol then
     f(t₁,....,tₙ) are terms.
